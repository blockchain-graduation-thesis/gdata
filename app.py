from fastapi import FastAPI, HTTPException
from fastapi.responses import StreamingResponse
import yfinance as yf
import matplotlib.pyplot as plt
import io
import uvicorn
app = FastAPI()

@app.get("/crypto")
async def get_crypto_data(crypto_currency: str = 'BTC', against_currency: str = 'USD', start: str = '2020-01-01', end: str = '2024-05-31'):
    # Define the ticker symbol for Yahoo Finance
    ticker = f'{crypto_currency}-{against_currency}'

    # Fetch the data
    data = yf.download(ticker, start=start, end=end)

    if data.empty:
        raise HTTPException(status_code=404, detail="No data found for the given parameters")

    # Create the plot
    plt.figure(figsize=(14, 7))
    plt.plot(data.index, data['Close'], label='Close Price')
    plt.title(f'{crypto_currency}/{against_currency} Closing Prices')
    plt.xlabel('Date')
    plt.ylabel('Price in USD')
    plt.legend()
    plt.grid(True)

    # Save the plot to a BytesIO object
    img = io.BytesIO()
    plt.savefig(img, format='png')
    img.seek(0)
    plt.close()

    # Return the image as a response
    return StreamingResponse(img, media_type="image/png")

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000)
