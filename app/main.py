from flask import Flask, request, jsonify,send_file
import yfinance as yf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import pickle
# openai.api_key = 'sk-proj-2dciRML28sHzn9ZrcrMJT3BlbkFJ4DF8ZGw2vT001PFrX6lp'
app = Flask(__name__)
# def get_response(data):
#     try:
#         print("Sending data to OpenAI API:")
#         response = openai.Completion.create(
#             model="text-davinci-003",
#             prompt=data + " Based on the prediction, give me a comment about the price",
#             max_tokens=1024,
#             n=1, 
#             temperature=0.5
#         )
#         print("Received response from OpenAI API:", response)
#         return response.choices[0].text.strip()
#     except Exception as e:
#         return str(e)

# Function to download data for a given cryptocurrency
def download_data(crypto_currency, against_currency='USD', start_date='2014-01-01', end_date='2024-08-11'):
    ticker = f'{crypto_currency}-{against_currency}'
    data = yf.download(ticker, start=start_date, end=end_date)
    return data

# Preprocess the data


# Prepare training data
def prepare_training_data(data, time_step=60):
    X, y = [], []
    for i in range(time_step, len(data)):
        X.append(data[i-time_step:i])
        y.append(data[i])
    return np.array(X), np.array(y)
def load_model():
    with open('trained_model.pkl', 'rb') as file:
        loaded_model = pickle.load(file)
    return loaded_model
def preprocess_data(data):
    scaler = MinMaxScaler()
    data['Close'] = scaler.fit_transform(data[['Close']])
    return data, scaler


# Create the model
# def create_model():
#     model = Sequential()
#     model.add(LSTM(units=50, return_sequences=True, input_shape=(60, 1)))
#     model.add(LSTM(units=50))
#     model.add(Dense(units=1))
#     model.compile(optimizer='adam', loss='mean_squared_error')
#     return model
def plot_predictions(actuals, predictions, currency):
    # Function to plot actual vs predicted prices
    plt.figure(figsize=(14, 7))
    plt.plot(actuals, label='Actual Prices')
    plt.plot(predictions, label='Predicted Prices')
    plt.title(f'{currency} Actual vs Predicted Prices')
    plt.xlabel('Time')
    plt.ylabel('Price (USD)')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    # plt.savefig('plot.png')  # Optionally save the plot
    # plot_url = base64_encode_image('plot.png')  # Optionally encode plot to base64
    plt.show()
# Endpoint to fetch data, train model, and predict
@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json()
    crypto_currency = data['currency']
    data = download_data(crypto_currency)
    data, scaler = preprocess_data(data)

    currency_data = data['Close'].values
    time_step = 60
    X, y = prepare_training_data(currency_data, time_step)
    X = X.reshape(X.shape[0], X.shape[1], 1)

    split = int(0.8 * len(X))
    X_train, X_test = X[:split], X[split:]
    y_train, y_test = y[:split], y[split:]

    model = load_model()
    model.fit(X_train, y_train, epochs=10, batch_size=32, verbose=0)

    y_pred = model.predict(X_test)
    y_pred_inv = scaler.inverse_transform(y_pred)
    y_test_inv = scaler.inverse_transform(y_test.reshape(-1, 1))
    response = {
        'currency': crypto_currency,
        # 'plot_url': f'data:image/png;base64,{plot_url}',
        'predictions': y_pred_inv.tolist(),
        'actuals': y_test_inv.tolist(),
    }             
    return jsonify(response)
# Ensure matplotlib uses a non-interactive backend suitable for server-side use
plt.switch_backend('Agg')

# Ensure matplotlib uses a non-interactive backend suitable for server-side use
plt.switch_backend('Agg')

@app.route('/visual', methods=['POST'])
def visual():
    # Receive JSON data from the request
    data = request.get_json()
    y_pred_inv = np.array(data['predictions'])
    y_test_inv = np.array(data['actuals'])

    # Create a plot in a thread-safe manner
    fig, ax = plt.subplots(figsize=(10, 6))
    ax.plot(y_test_inv, marker='o', linestyle='-', color='b', label='Actual Prices')
    ax.plot(y_pred_inv, marker='o', linestyle='--', color='r', label='Predicted Prices')
    ax.set_title('Actual vs Predicted Prices')
    ax.set_xlabel('Time')
    ax.set_ylabel('Price')
    ax.legend()
    ax.grid(True)
    fig.tight_layout()

    # Save the plot to a PNG file
    plot_filename = 'plot.png'
    fig.savefig(plot_filename)
    plt.close(fig)  # Close the plot to release resources

    # Optionally, return the plot file as a response
    return send_file(plot_filename, mimetype='image/png')
if __name__ == '__main__':
    print("Running")
    app.run(host='0.0.0.0', port=5001, debug=True)
