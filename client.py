import grpc
import crypto_pb2
import crypto_pb2_grpc
import matplotlib.pyplot as plt
import io

def run():
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = crypto_pb2_grpc.CryptoServiceStub(channel)
        response = stub.GetCryptoData(crypto_pb2.CryptoRequest(
            crypto_currency='BTC',
            against_currency='ETH',
            start_date='2020-01-01',
            end_date='2024-05-31'
        ))

        if response.image:
            img = io.BytesIO(response.image)
            img.seek(0)
            plt.imshow(plt.imread(img))
            plt.axis('off')
            plt.show()
        else:
            print("No data found for the given parameters")

if __name__ == '__main__':
    run()
