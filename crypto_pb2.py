# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: crypto.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x0c\x63rypto.proto\"h\n\rCryptoRequest\x12\x17\n\x0f\x63rypto_currency\x18\x01 \x01(\t\x12\x18\n\x10\x61gainst_currency\x18\x02 \x01(\t\x12\x12\n\nstart_date\x18\x03 \x01(\t\x12\x10\n\x08\x65nd_date\x18\x04 \x01(\t\"\x1f\n\x0e\x43ryptoResponse\x12\r\n\x05image\x18\x01 \x01(\x0c\x32\x43\n\rCryptoService\x12\x32\n\rGetCryptoData\x12\x0e.CryptoRequest\x1a\x0f.CryptoResponse\"\x00\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'crypto_pb2', _globals)
if not _descriptor._USE_C_DESCRIPTORS:
  DESCRIPTOR._loaded_options = None
  _globals['_CRYPTOREQUEST']._serialized_start=16
  _globals['_CRYPTOREQUEST']._serialized_end=120
  _globals['_CRYPTORESPONSE']._serialized_start=122
  _globals['_CRYPTORESPONSE']._serialized_end=153
  _globals['_CRYPTOSERVICE']._serialized_start=155
  _globals['_CRYPTOSERVICE']._serialized_end=222
# @@protoc_insertion_point(module_scope)
