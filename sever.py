from concurrent import futures
import grpc
import yfinance as yf
import matplotlib.pyplot as plt
import io
import crypto_pb2
import crypto_pb2_grpc
import threading

class CryptoService(crypto_pb2_grpc.CryptoServiceServicer):
    def __init__(self):
        self.lock = threading.Lock()

    def save_plot(self, crypto_currency, against_currency, start_date, end_date):
        with self.lock:
            # Define the ticker symbol for Yahoo Finance
            ticker = f'{crypto_currency}-{against_currency}'

            # Fetch the data
            data = yf.download(ticker, start=start_date, end=end_date)

            if data.empty:
                return None

            # Create the plot
            plt.figure(figsize=(14, 7))
            plt.plot(data.index, data['Close'], label='Close Price')
            plt.title(f'{crypto_currency}/{against_currency} Closing Prices')
            plt.xlabel('Date')
            plt.ylabel('Price in USD')
            plt.legend()
            plt.grid(True)

            # Save the plot to a BytesIO object
            img = io.BytesIO()
            plt.savefig(img, format='png')
            img.seek(0)
            plt.close()
            return img.read()

    def GetCryptoData(self, request, context):
        image_data = self.save_plot(
            request.crypto_currency, 
            request.against_currency, 
            request.start_date, 
            request.end_date
        )

        if image_data is None:
            context.set_details("No data found for the given parameters")
            context.set_code(grpc.StatusCode.NOT_FOUND)
            return crypto_pb2.CryptoResponse()

        return crypto_pb2.CryptoResponse(image=image_data)

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    crypto_pb2_grpc.add_CryptoServiceServicer_to_server(CryptoService(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    serve()
