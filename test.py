from flask import Flask, request, jsonify
from openai import OpenAI

client = OpenAI(api_key='sk-proj-2dciRML28sHzn9ZrcrMJT3BlbkFJ4DF8ZGw2vT001PFrX6lp')

# Set your OpenAI API key

app = Flask(__name__)

@app.route('/')
def index():
    return "Welcome to the GPT-3 Chat App"

@app.route('/chat', methods=['POST'])
def chat():
    data = request.get_json()
    user_message = data.get("message", "")

    if not user_message:
        return jsonify({"error": "Message field is empty"}), 400

    try:
        response = client.completions.create(model="gpt-3.5-turbo",
        prompt=user_message,
        max_tokens=150,
        n=1,
        stop=None,
        temperature=0.7)

        reply = response.choices[0].text.strip()
        return jsonify({"response": reply})
    except Exception as e:
        return jsonify({"error": str(e)}), 500

if __name__ == '__main__':
    app.run(debug=True)
